$(function() {
	$('.info__item').each(function(){
		$(this).click(function(evt){
			evt.preventDefault();
			if(!$(this).hasClass("is-open")){
				$(this).addClass("is-open");
			}else{
				$(this).removeClass("is-open");
			}
		});
	});
})