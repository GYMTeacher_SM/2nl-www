

var host = "http://ec2-13-125-5-65.ap-northeast-2.compute.amazonaws.com:8080";
//var host = "http://localhost:8080";
var errorMsg="요청을 처리할 수 없습니다.";
var mixin = {
    ajax:{
        methods:{
            POST:function(){
                _this=this;
                $.ajax({
                    url: host+_this.request_data.url,
                    type: _this.request_data.type,
                    data: JSON.stringify(_this.request_data.data),
                    contentType: "application/json",
                    timeout : 30000,
                    success : function(response){
                        _this.success(response);},
                    error  : function(request){
                        _this.fail(request);
                    }
                })
            },
            GET:function(){
                _this=this;
                $.ajax({
                    url: host+_this.request_data.url,
                    type: _this.request_data.type,
                    contentType: "application/json",
                    timeout : 30000,
                    success : function(response){
                        _this.success(response);},
                    error  : function(request){
                        _this.fail(request);
                    }
                })
            }
        },
    }
}



var term = function(termsId){
    return {
    el:'#term',
    mixins: [mixin.ajax],
    data:{
        seen: false,
        acceptTerms : "",
        acceptPrivacyPolicy:"",
        request_data:{
            url:'/v1.0/terms/info/'+termsId, // 호출 url
            type: "GET",
            data:{ // 요청 데이터
                termsId:termsId
            }
        }
    },
    methods: {
        success : function(response){
            if(response.result==200){
                this.seen=true;
                    this.acceptTerms = response.value.acceptTerms;
                    this.acceptPrivacyPolicy = response.value.acceptPrivacyPolicy;



            }
        },
        fail : function(request){
            alert(errorMsg);
        }
    }
}};

var auth_success =function(authToken){
    return {
    el:'#auth_success',
    mixins: [mixin.ajax],
    data:{
        seen: false,
        request_data:{
            url:'/v1.0/user/email/activate', // 호출 url
            type: "POST",
            data:{ // 요청 데이터
                token:authToken
            }
        }
    },
    methods: {
        success : function(response){
            if(response.result==200){
                this.seen=true;
            }
        },
        fail : function(request){
            var errorObject =  request.responseJSON;
            var code = errorObject.code;
            if(code){
                if(code==1002){
                    alert("신고된 이메일 계정입니다. 고객센터에 문의해 주세요.");
                }else if(code==1004){
                    alert("링크가 만료되어 있습니다. 다시 가입해 주세요.");
                }else if(code==1502){
                    alert("토큰이 유효하지 않습니다.");
                }
            }else{
                alert(errorMsg);
            }
        }
    }
}};

var auth_report = function(authToken){
    return {
    el:'#auth_report',
    mixins: [mixin.ajax],
    data:{
        seen: false,
        request_data:{
            url:'/v1.0/user/email/report', // 호출 url
            type: "POST",
            data:{ // 요청 데이터
                token:authToken
            }
        }
    },
    methods: {
        success : function(response){
            if(response.result==200){
                this.seen=true;
            }else{
                alert(response.message);
            }
        },
        fail : function(request){
            var errorObject =  request.responseJSON;
            var code = errorObject.code;
            if(code){
               if(code==1002){
                   alert("인증이 완료된 계정입니다. 고객센터에 문의해 주세요.");
               }else if(code==1004){
                   alert("링크가 만료되어 있습니다. 다시 가입해 주세요.");
               }else if(code==1502){
                   alert("토큰이 유효하지 않습니다.");
               }
            }else{
                alert(errorMsg);
            }

        }
    }
}};
